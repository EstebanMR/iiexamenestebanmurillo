/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiexamenestebanmurilloa;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author muril
 */
public class Tablero {

    private int x;
    private int y;
    private int t;
    private int est;
    private Color color;

    public Tablero() {
        t = 70;
    }

    public Tablero(int x, int y, int est) {
        this.x = x;
        this.y = y;
        this.est = est;
        t = 70;
        color = Color.WHITE;
    }

    void paint(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(x, y, t, t);
        g.setColor(color);
        g.fillOval(x + 5, y + 5, t - 10, t - 10);
    }

    public int getEst() {
        return est;
    }

    public int getT() {
        return t;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setEst(int est) {
        this.est = est;
    }

    public void setT(int t) {
        this.t = t;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
