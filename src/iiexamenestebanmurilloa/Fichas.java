/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiexamenestebanmurilloa;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author muril
 */
public class Fichas {

    int x;
    int y;
    private int t;
    private Color color;
    private int s;
    private int j;

    public Fichas() {
    }

    public Fichas(int x, int y, Color color, int s, int j) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.s = s;
        this.j = j;
        t = 60;
    }

    public Fichas(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
        t = 60;
    }

    public Color getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getS() {
        return s;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }
    

    void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, t, t);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, t, t + 6);
    }

    public Rectangle getBounds1() {
        return new Rectangle(x-6, y+6, t+6, t + 6);
    }
}
