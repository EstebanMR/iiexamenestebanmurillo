/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iiexamenestebanmurilloa;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author muril
 */
public class CuatroL extends JPanel implements KeyListener {

    private Tablero t;
    private Tablero[][] ta;
    private int i;
    private int pos;
    private int turno;
    private LinkedList<Fichas> f;
    private Color color1;
    private Color color2;
    Fichas fic;
    private int in;
    private int j1;
    private int j2;

    public CuatroL() {
        fic = new Fichas();
        color1 = Color.YELLOW;
        color2 = Color.red;
        f = new LinkedList<>();
        t = new Tablero();
        ta = new Tablero[6][7];
        setFocusable(true);
        addKeyListener(this);
    }

    public void llenart() {
        int x = 0;
        int y = 70;
        for (int i = 0; i < ta.length; i++) {
            for (int j = 0; j < ta[i].length; j++) {
                    ta[i][j] = new Tablero(x, y, 0);
                    x += 70;
                
            }
            x = 0;
            y += 70;
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        if (i == 1) {
            if (in == 0) {
                llenart();
                in = 1;
            }
            for (int i = 0; i < ta.length ; i++) {
                for (int j = 0; j < ta[i].length; j++) {
                    ta[i][j].paint(g);
                }
            }
            g.setFont(new Font("cambri", Font.BOLD, 15));
            g.setColor(Color.BLACK);
            g.drawString("Jugador " + turno, 490, 12);
            g.drawString("pts jugador 1: "+j1, 440, 27);
            g.drawString("pts jugador 2: "+j2, 440, 46);
            for (Fichas fi : f) {
                fi.paint(g);
                soltar(fi);

            }
        } else {
            g.setFont(new Font("ARIAL", Font.BOLD, 25));
            g.drawString("Bienvenido a 4 en linea", 120, 230);
            g.setFont(new Font("negrita", Font.BOLD, 10));
            g.drawString("Presine 'i' para entrar o salir, 'p' para pausar, 'r' para reiniciar y los numeros del 1 al 7 para jugar", 40, 280);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(560, 500);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 73) {
            if (i == 0) {
                i = 1;
                turno = 1;
            } else if (i == 1) {
                turno = 0;
                i = 0;
            }
        } else if (e.getKeyCode() == 82) {
            f.clear();
            llenart();
            j1 = 0;
            j2 = 0;

        } else if (e.getKeyCode() == 49 || e.getKeyCode() == 50 || e.getKeyCode() == 51 || e.getKeyCode() == 52 || e.getKeyCode() == 53 || e.getKeyCode() == 54 || e.getKeyCode() == 55) {
            switch (e.getKeyCode()) {
                case 49:
                    pos = 6;
                    break;
                case 50:
                    pos = 1 * 75;
                    break;
                case 51:
                    pos = 2 * 72;
                    break;
                case 52:
                    pos = 3 * 72;
                    break;
                case 53:
                    pos = 4 * 71;
                    break;
                case 54:
                    pos = 5 * 71;
                    break;
                case 55:
                    pos = 6 * 71;
                    break;
                default:
                    break;
            }
            if (turno == 1) {
                f.add(new Fichas(pos, 5, color1, 1, turno));
                turno = 2;
            } else if (turno == 2) {
                f.add(new Fichas(pos, 5, color2, 1, turno));
                turno = 1;
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    private void soltar(Fichas fic) {
        int ty = fic.y;
        if (fic.getS() == 1) {
            fic.y += 5;
        }

        for (Fichas fi : f) {
            if (fi.y > 420) {
                fic.setS(2);
                fic.y = ty;
            }
            if (fi.getS() == 2 && fic.getBounds().intersects(fi.getBounds())) {
                fic.setS(2);
                fic.y = ty;
            }

        }
    }


}
